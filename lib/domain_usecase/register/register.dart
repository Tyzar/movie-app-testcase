import 'package:majootestcase/repositories/session/session_repo.dart';
import 'package:majootestcase/repositories/users/user_repo.dart';
import 'package:majootestcase/domain_usecase/user.dart';

class Register{
  final UserRepo _userRepo;
  final SessionRepo _sessionRepo;

  Register(this._userRepo, this._sessionRepo);

  String? _hasInvalid(User user){
    //alt path 2:Form empty
    if(user.userName.isEmpty
        || user.email.isEmpty
        || user.password.isEmpty){
      return "Form tidak boleh kosong. Mohon cek kembali data yang anda inputkan";
    }

    //alt path 1: Cek email pattern
    final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
    if(!pattern.hasMatch(user.email)){
      return "Masukkan e-mail yang valid";
    }

    return null;
  }

  Future doRegister(User user) async{
    String? errMsg = _hasInvalid(user);
    if(errMsg != null){
      throw errMsg;
    }

    try{
      await _userRepo.registerUser(user);
      //save to session
      await _sessionRepo.save(user);
    }catch(e){
      throw e;
    }
  }
}
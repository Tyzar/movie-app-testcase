import 'dart:io';

import 'package:majootestcase/repositories/movies/movie_repo.dart';
import 'package:majootestcase/domain_usecase/movie.dart';
import 'package:majootestcase/utils/connectivity.dart';

class GetMovies{
  final MovieRepo _movieRepo;

  GetMovies(this._movieRepo);

  Future<List<Movie>> doGetMovies() async{
    //check internet conn
    bool isConnect = await hasConnectivity();
    if(!isConnect){
      throw SocketException("Anda tidak terkoneksi dengan internet");
    }

    //if has conn, fetch
    return _movieRepo.getListMovies();
  }

}
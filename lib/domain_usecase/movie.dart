class Movie {
  final String id;
  final String title;
  final String poster;
  final int rank;
  final int year;
  final List<MovieSeries> series;

  Movie({required this.id,required this.year,required this.series, required this.title, required this.poster,required this.rank });
}

class MovieSeries{
  final String id;
  final String title;
  final String poster;

  MovieSeries({required this.id,required this.title,required this.poster});
}
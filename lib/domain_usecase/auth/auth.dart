import 'package:majootestcase/repositories/users/user_repo.dart';
import 'package:majootestcase/domain_usecase/user.dart';
import 'package:majootestcase/repositories/session/session_repo.dart';

class Auth{
  final SessionRepo _sessionRepo;
  final UserRepo _userRepo;

  Auth(this._sessionRepo,this._userRepo);

  Future<User?> getLastSession() async{
    return _sessionRepo.getLastSession();
  }

  String? hasInvalid(User user){
    //alt path 2: Cek form empty
    if(user.email.isEmpty || user.password.isEmpty){
      return "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan";
    }

    //alt path 1: Cek email pattern
    final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
    if(!pattern.hasMatch(user.email)){
      return "Masukkan e-mail yang valid";
    }

    return null;
  }

  Future login(User user) async{
    //validate input
    String? errMsg = hasInvalid(user);
    if(errMsg != null){
      throw errMsg;
    }

    //validate on user db
    User? result = await _userRepo.login(user.email,user.password);
    if(result == null){
      throw "Login gagal. Periksa kembali inputan anda atau daftar akun baru.";
    }

    //if has record, save session
    return _sessionRepo.save(user);
  }
}
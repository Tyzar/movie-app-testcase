import 'package:flutter/material.dart';
import 'package:majootestcase/domain_usecase/movie.dart';

class DetailMoviePage extends StatelessWidget {
  final Movie movie;

  const DetailMoviePage({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Detail Movie")),
        body: Padding(
          padding: EdgeInsets.only(top: 20, left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              movieHeader(),
              movie.series.isNotEmpty?Container(
                margin: EdgeInsets.only(top: 20, bottom: 16),
                child: Text("Series",
                    style:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
              ):SizedBox(),
              movie.series.isNotEmpty?movieSeries():SizedBox(),
            ],
          ),
        ));
  }

  Widget movieHeader() {
    return Flexible(
        child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: 150,
          color: Colors.blue,
          child: AspectRatio(
              aspectRatio: 9 / 16,
              child: Image.network(
                movie.poster,
                fit: BoxFit.cover,
              )),
        ),
        Expanded(
            child: Container(
          margin: EdgeInsets.only(left: 8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 16),
                child: Text(
                  movie.title,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 8),
                child: Text(
                  "Tahun : " + (movie.year == 0?"-":movie.year.toString()),
                  style: TextStyle(fontSize: 16),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 16),
                child: Text(
                  "Rank : " + movie.rank.toString(),
                  style: TextStyle(fontSize: 16),
                ),
              )
            ],
          ),
        ))
      ],
    ));
  }

  Widget movieSeries() {
    return Flexible(
        child: ListView.builder(
            itemCount: movie.series.length,
            itemBuilder: (context, idx) => seriesItem(idx)));
  }

  Widget seriesItem(int idx) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 16),
            width: 150,
            child: AspectRatio(
              aspectRatio: 16 / 9,
              child: Image.network(movie.series[idx].poster, fit: BoxFit.cover),
            ),
          ),
          Expanded(
            child: Container(
                margin: EdgeInsets.only(left: 8, top: 20),
                child: Text(
                  movie.series[idx].title,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                )),
          )
        ],
      ),
    );
  }
}

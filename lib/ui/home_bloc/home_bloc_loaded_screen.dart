import 'package:flutter/material.dart';
import 'package:majootestcase/domain_usecase/movie.dart';
import 'package:majootestcase/ui/detail_movie/detail_movie_page.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Movie> data;

  const HomeBlocLoadedScreen({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: 20,bottom: 0,left: 20,right: 20),
        child: ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, index) {
            return movieItemWidget(context, data[index]);
          },
        ),
      ),
    );
  }

  Widget movieItemWidget(BuildContext context, Movie data) {
    return InkWell(
      onTap: () {
        goToDetail(context, data);
      },
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0))),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(left: 25,right: 25, top:25),
              child: Image.network(data.poster),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 12, horizontal: 25),
              child: Text(
                data.title,
                textDirection: TextDirection.ltr,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
              ),
            )
          ],
        ),
      ),
    );
  }

  void goToDetail(BuildContext context, Movie data) {
    Navigator.push(context,
        MaterialPageRoute(builder: (_) => DetailMoviePage(movie: data)));
  }
}

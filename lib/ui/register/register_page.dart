import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/bloc/register_bloc/register_bloc.dart';
import 'package:majootestcase/bloc/register_bloc/register_bloc_state.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/repositories/movies/movie_repo.dart';
import 'package:majootestcase/domain_usecase/user.dart';
import 'package:majootestcase/domain_usecase/get_movies/get_movies.dart';
import 'package:majootestcase/repositories/movies/movie_repo_impl.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';

class RegisterPage extends StatefulWidget {
  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _emailController = TextController(initialValue: "");
  final _usernameController = TextController(initialValue: "");
  final _passwordController = TextController(initialValue: "");
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<RegisterBloc, RegisterBlocState>(
        listener: (context, state) {
          if (state is RegisterBlocErrorState) {
            showInfo((state).errMsg);
          } else if (state is RegisterBlocSuccessState) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (_) => HomeBlocCubit(
                      GetMovies(RepositoryProvider.of<MovieRepoImpl>(context)))
                    ..fetching_data(),
                  child: HomeBlocScreen(),
                ),
              ),
            );
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Daftar Pengguna',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Daftarkan diri anda',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: CustomButton(
                    text: 'Daftar',
                    onPressed: handleRegister,
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
          ),
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            hint: 'Username anda',
            label: 'Username',
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleRegister() {
    var regisBloc = BlocProvider.of<RegisterBloc>(context);
    final _email = _emailController.value;
    final _username = _usernameController.value;
    final _password = _passwordController.value;
    User user = User(email: _email, userName: _username, password: _password);
    regisBloc.registerUser(user);
  }

  void showInfo(String errMsg) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(errMsg),
      duration: Duration(seconds: 2),
    ));
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/bloc/register_bloc/register_bloc.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/repositories/movies/movie_repo.dart';
import 'package:majootestcase/repositories/movies/movie_repo_impl.dart';
import 'package:majootestcase/repositories/session/session_repo.dart';
import 'package:majootestcase/repositories/session/session_repo_impl.dart';
import 'package:majootestcase/repositories/users/user_repo.dart';
import 'package:majootestcase/domain_usecase/user.dart';
import 'package:majootestcase/domain_usecase/get_movies/get_movies.dart';
import 'package:majootestcase/domain_usecase/register/register.dart';
import 'package:majootestcase/repositories/users/user_repo_impl.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/register/register_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController(initialValue: "");
  final _passwordController = TextController(initialValue: "");
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocLoggedInState) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) =>
                      HomeBlocCubit(GetMovies(context.read<MovieRepoImpl>()))
                        ..fetching_data(),
                  child: HomeBlocScreen(),
                ),
              ),
            );
          } else if (state is AuthBlocErrorState) {
            showInfo(state.error);
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan login terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: CustomButton(
                    text: 'Login',
                    onPressed: handleLogin,
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                _register(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => BlocProvider(
                        create: (_) {
                          UserRepo userRepo = RepositoryProvider.of<UserRepoImpl>(context);
                          SessionRepo sessionRepo = RepositoryProvider.of<SessionRepoImpl>(context);
                          var registerLogic = Register(userRepo,sessionRepo);
                          return RegisterBloc(registerLogic);
                        },
                        child: RegisterPage(),
                      )));
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  void handleLogin() async {
    var authBlocCubit = BlocProvider.of<AuthBlocCubit>(context);

    final _email = _emailController.value;
    final _password = _passwordController.value;

    User user = User(
      email: _email,
      userName: '',
      password: _password,
    );
    authBlocCubit.login_user(user);
  }

  void showInfo(String errMsg) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(errMsg),
      duration: Duration(seconds: 2),
    ));
  }
}

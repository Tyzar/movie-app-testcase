import 'package:flutter/material.dart';

class ErrorNetworkScreen extends StatelessWidget{
  final Function() retry;

  const ErrorNetworkScreen({Key? key,required this.retry}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Periksa koneksi internet anda",
              style: TextStyle(
                  fontSize: 20, color: Colors.black),
            ),
            Icon(Icons.wifi_off,size: 100),
            Container(
              margin: EdgeInsets.only(top: 30),
              child: IconButton(iconSize: 48,icon: Icon(Icons.refresh,color: Colors.black54,),onPressed: (){
                retry();
              },),
            )
          ],
        ),
      ),
    );
  }

}
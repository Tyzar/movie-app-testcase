import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Function() retry;
  final Color? textColor;
  final double fontSize;
  final double gap;
  final Widget? retryButton;

  const ErrorScreen(
      {Key? key,
      this.gap = 10,
      this.retryButton,
      required this.message,
      this.fontSize = 14,
      required this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                message,
                style: TextStyle(
                    fontSize: 20, color: Colors.black),
              ),
              Container(
                margin: EdgeInsets.only(top: 30),
                child: IconButton(iconSize: 48,icon: Icon(Icons.refresh,color: Colors.black54,),onPressed: (){
                  retry();
                },),
              )
            ],
          ),
      ),
    );
  }
}

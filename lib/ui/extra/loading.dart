import 'package:flutter/material.dart';

class LoadingIndicator extends StatefulWidget {
  final double height;
  final Color color;

  const LoadingIndicator({Key? key, this.height =10,required this.color}) : super(key: key);

  @override
  State<LoadingIndicator> createState() => _LoadingIndicatorState();
}

class _LoadingIndicatorState extends State<LoadingIndicator> with TickerProviderStateMixin {
  late AnimationController controller;

  void initState() {
    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    )..addListener(() {
      setState(() {});
    });
    controller.repeat(reverse: true);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(value: controller.value, strokeWidth: 100,color: widget.color),
      ),
    );
  }
}

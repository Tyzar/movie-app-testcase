import 'package:flutter/material.dart';

class Splashscreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          "Movie App",
          style: TextStyle(
              fontWeight: FontWeight.w500, fontSize: 40, color: Colors.blue),
        ),
      ),
    );
  }
}

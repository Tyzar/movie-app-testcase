import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class AppDb{

  final String dbName = "testcase.db";
  final int dbVersion = 1;

  AppDb._privateConstructor();
  static final AppDb instance = AppDb._privateConstructor();

  static Database? _db;

  Future<Database> get db async{
    if(_db != null){
      return _db!;
    }

    String dbPath = await getDatabasesPath();
    String appDbPath = join(dbPath,dbName);

    try{
      _db = await openDatabase(appDbPath,version: dbVersion, onCreate: onCreate);
      return _db!;
    }
    catch(e){
      throw "Gagal membuka database";
    }
  }

  FutureOr<void> onCreate(Database db, int version) async {
    await createUserTable(db);
  }

  Future createUserTable(Database db) async{
    const String sql = "create table user ("
        "email text primary key,"
        "username text not null,"
        "password text not null"
        " )";
    await db.execute(sql);
  }
}
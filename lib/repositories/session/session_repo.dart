import 'package:majootestcase/domain_usecase/user.dart';

abstract class SessionRepo{
  Future save(User user);
  Future<User?> getLastSession();
}
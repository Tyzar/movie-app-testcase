import 'dart:convert';

import 'package:majootestcase/domain_usecase/user.dart';
import 'package:majootestcase/repositories/session/session_repo.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../domain_usecase/user.dart';

class SessionRepoImpl implements SessionRepo{

  @override
  Future save(User user) async{
    var sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in",true);
    String data = jsonEncode(user.toJson());
    sharedPreferences.setString("user_value", data);
  }

  @override
  Future<User?> getLastSession() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    var isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if(isLoggedIn == null || !isLoggedIn){
      return null;
    }

    var sessData = sharedPreferences.getString("user_value");
    print(sessData);
    if(sessData == null || sessData.isEmpty){
      return null;
    }

    return User.fromJson(jsonDecode(sessData));
  }

}
import 'package:majootestcase/domain_usecase/user.dart';

abstract class UserRepo{
  Future<User?> login(String email,String password);
  Future registerUser(User user);
}
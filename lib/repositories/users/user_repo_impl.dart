import 'package:majootestcase/repositories/sqlitedb/app_db.dart';
import 'package:majootestcase/repositories/users/user_repo.dart';
import 'package:majootestcase/domain_usecase/user.dart';

class UserRepoImpl implements UserRepo{

  @override
  Future<User?> login(String email, String password) async {
    var db = await AppDb.instance.db;
    var result = await db.query("user",columns: ['email','username']
        ,where: 'email = ? and password = ?'
        ,whereArgs: [email,password]
        ,limit: 1
    );

    if(result.isEmpty){
      return null;
    }

    var record = result.first;
    return new User(email: record['email'] == null?"":record['email'].toString()
        , userName: record['username'] == null? "" : record['username'].toString()
        , password: '');
  }

  @override
  Future registerUser(User user) async{
    var db = await AppDb.instance.db;
    await db.transaction((txn) async {
      var insertVal = {
        'email': user.email,
        'username': user.userName,
        'password': user.password
      };
      await txn.insert("user", insertVal);
    });
  }

}
import 'dart:async';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class DioClient{

  DioClient._privateConstructors();
  static final DioClient instance = DioClient._privateConstructors();

  static Dio? _dioInstance;

  Future<Dio> _createInstance() async {

    var options = BaseOptions(
        baseUrl: "https://imdb8.p.rapidapi.com/auto-complete?q=game%20of%20thr",
        connectTimeout: 12000,
        receiveTimeout: 12000,
        headers: {
          "x-rapidapi-key": "08e9318168msh1eb8beda128443cp170cdejsn8cadee24a0c8",
          "x-rapidapi-host": "imdb8.p.rapidapi.com"
        });
    var dio = new Dio(options);
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
    return dio;
  }

  Future<Dio> get dio async {
    if(_dioInstance != null){
      return _dioInstance!;
    }

    _dioInstance = await _createInstance();
    return _dioInstance!;
  }
}


import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/repositories/http_dio/dio_client.dart';
import 'package:majootestcase/repositories/movies/movie_repo.dart';
import 'package:majootestcase/domain_usecase/movie.dart';
import 'package:majootestcase/repositories/movies/movie_response.dart';

class MovieRepoImpl implements MovieRepo{

  @override
  Future<List<Movie>> getListMovies() async {
    try {
      var dio = await DioClient.instance.dio;
      Response<String> response  = await dio.get("");
      if(response.statusCode != 200){
        throw "Gagal mendapatkan data movie";
      }

      if(response.data == null){
        throw "Terjadi kesalahan data";
      }

      MovieResponse movieResponse = MovieResponse.fromJson(jsonDecode(response.data!));
      List<Movie> movies = [];
      for(var data in movieResponse.data){
        List<MovieSeries> movSeries = [];
        if(data.series.isNotEmpty){
          for(var serie in data.series){
            movSeries.add(MovieSeries(id: serie.id
                , title: serie.l
                , poster: serie.i.imageUrl));
          }
        }

        Movie movie = Movie(id: data.id, year: data.year
            , series: movSeries, title: data.l
            , poster: data.i.imageUrl, rank: data.rank);
        movies.add(movie);
      }
      return movies;
    } catch(e) {
      print(e.toString());
      throw e;
    }
  }
}
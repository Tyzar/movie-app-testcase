import 'package:majootestcase/domain_usecase/movie.dart';

abstract class MovieRepo{
  Future<List<Movie>> getListMovies();
}
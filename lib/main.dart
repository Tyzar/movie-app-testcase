import 'package:majootestcase/bloc/check_session_bloc/check_session_bloc.dart';
import 'package:majootestcase/bloc/check_session_bloc/session_state.dart';
import 'package:majootestcase/repositories/movies/movie_repo.dart';
import 'package:majootestcase/repositories/movies/movie_repo_impl.dart';
import 'package:majootestcase/repositories/session/session_repo_impl.dart';
import 'package:majootestcase/repositories/session/session_repo.dart';
import 'package:majootestcase/repositories/users/user_repo_impl.dart';
import 'package:majootestcase/repositories/users/user_repo.dart';
import 'package:majootestcase/domain_usecase/auth/auth.dart';
import 'package:majootestcase/domain_usecase/get_movies/get_movies.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:flutter/foundation.dart';
import 'package:majootestcase/ui/splashscreen.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/home_bloc/home_bloc_cubit.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
        providers: [
          RepositoryProvider(create: (context) => SessionRepoImpl()),
          RepositoryProvider(create: (context) => UserRepoImpl()),
          RepositoryProvider(create: (context) => MovieRepoImpl())
        ],
        child: MaterialApp(
          title: 'Flutter Demo',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: BlocProvider(
            create: (context) {
              var authLogic = Auth(context.read<SessionRepoImpl>(),
                  context.read<UserRepoImpl>());
              return CheckSessionBloc(authLogic)..checkSession();
            },
            child: MyHomePageScreen(),
          ),
        ));
  }
}

class MyHomePageScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CheckSessionBloc, SessionState>(
        builder: (context, state) {
      if (state is NoSessionState) {
        return BlocProvider(
          create: (context) {
            var authLogic = Auth(
                context.read<SessionRepoImpl>(), context.read<UserRepoImpl>());
            return AuthBlocCubit(authLogic);
          },
          child: LoginPage(),
        );
      } else if (state is HasLastSessionState) {
        return BlocProvider(
          create: (context) =>
              HomeBlocCubit(GetMovies(context.read<MovieRepoImpl>()))
                ..fetching_data(),
          child: HomeBlocScreen(),
        );
      } else {
        return Splashscreen();
      }
    });
  }
}

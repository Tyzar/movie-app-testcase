import 'package:equatable/equatable.dart';

class RegisterBlocState extends Equatable{
  const RegisterBlocState();

  @override
  List<Object?> get props => [];
}

class RegisterBlocInitState extends RegisterBlocState{}

class RegisterBlocLoadingState extends RegisterBlocState{}

class RegisterBlocErrorState extends RegisterBlocState{
  final String errMsg;

  const RegisterBlocErrorState(this.errMsg);

  @override
  List<Object?> get props => [errMsg];
}

class RegisterBlocSuccessState extends RegisterBlocState{}
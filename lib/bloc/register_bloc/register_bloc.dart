import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/register_bloc/register_bloc_state.dart';
import 'package:majootestcase/domain_usecase/user.dart';
import 'package:majootestcase/domain_usecase/register/register.dart';

class RegisterBloc extends Cubit<RegisterBlocState>{
  final Register registerLogic;
  
  RegisterBloc(this.registerLogic) : super(RegisterBlocInitState());
  
  void registerUser(User user) async{
    emit(RegisterBlocLoadingState());
    try{
      await registerLogic.doRegister(user);
      emit(RegisterBlocSuccessState());
    }
    catch(e){
      emit(RegisterBlocErrorState(e.toString()));
    }
  }
}
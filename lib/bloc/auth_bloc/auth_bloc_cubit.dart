import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/domain_usecase/auth/auth.dart';
import 'package:majootestcase/domain_usecase/user.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  final Auth _auth;

  AuthBlocCubit(this._auth) : super(AuthBlocInitialState());

  Future login_user(User user) async{
    emit(AuthBlocLoadingState());
    try{
      await _auth.login(user);
      emit(AuthBlocLoggedInState());
    }
    catch(e){
      emit(AuthBlocErrorState(e.toString()));
    }
  }
}

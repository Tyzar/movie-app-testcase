import 'package:equatable/equatable.dart';

abstract class SessionState extends Equatable{
  const SessionState();
  @override
  List<Object?> get props => [];
}

class SessionInitState extends SessionState{}

class HasLastSessionState extends SessionState{}

class NoSessionState extends SessionState{}
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/check_session_bloc/session_state.dart';
import 'package:majootestcase/domain_usecase/auth/auth.dart';

class CheckSessionBloc extends Cubit<SessionState>{
  final Auth _auth;

  CheckSessionBloc(this._auth) : super(SessionInitState());

  Future checkSession() async{
    var session = await _auth.getLastSession();
    if(session == null){
      emit(NoSessionState());
    }
    else{
      emit(HasLastSessionState());
    }
  }
}
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/domain_usecase/get_movies/get_movies.dart';
import 'package:majootestcase/domain_usecase/movie.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  final GetMovies _getMovies;

  HomeBlocCubit(this._getMovies) : super(HomeBlocInitialState());

  void fetching_data() async {
    emit(HomeBlocInitialState());
    emit(HomeBlocLoadingState());
    try{
      var result = await _getMovies.doGetMovies();
      emit(HomeBlocLoadedState(result));
    }
    catch(e){
      if(e is SocketException){
        emit(HomeBlocConnErrorState());
      }
      else{
        emit(HomeBlocErrorState(e.toString()));
      }
    }
  }

}
